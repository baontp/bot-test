import logging
import logging
import sys
from bot.bot_helper import update_config_data
import config
from bot import bot, dispatcher
from webapp import WebApp
from config import (BASE_WEBHOOK_URL, WEBHOOK_PATH,
                    WEBHOOK_SECRET, WEBHOOK_SSL_CERT,
                    WEB_SERVER_HOST,
                    WEB_SERVER_PORT, WEBHOOK_SSL_CERT, WEBHOOK_SSL_PRIV)
from aiogram.webhook.aiohttp_server import SimpleRequestHandler, setup_application
from aiogram.types import FSInputFile
from aiogram import Bot


async def on_startup(bot: Bot) -> None:
    await update_config_data(bot)

    # If you have a self-signed SSL certificate, then you will need to send a public
    # certificate to Telegram
    await bot.set_webhook(f"{BASE_WEBHOOK_URL}{WEBHOOK_PATH}",
                          certificate=FSInputFile(
                              WEBHOOK_SSL_CERT) if config.SELF_SIGN else None,
                          secret_token=WEBHOOK_SECRET)


######################    MAIN    ######################
def main() -> None:
    # Init
    webApp = WebApp(bot=bot)

    # Register startup hook to initialize webhook
    dispatcher.startup.register(on_startup)

    # Create an instance of request handler,
    # aiogram has few implementations for different cases of usage
    # In this example we use SimpleRequestHandler which is designed to handle simple cases
    webhook_requests_handler = SimpleRequestHandler(
        dispatcher=dispatcher,
        bot=bot,
        secret_token=WEBHOOK_SECRET,
    )
    # Register webhook handler on application
    webhook_requests_handler.register(webApp.app, path=WEBHOOK_PATH)

    # Mount dispatcher startup and shutdown hooks to aiohttp application
    setup_application(webApp.app, dispatcher, bot=bot)

    # Run
    if config.SELF_SIGN:
        webApp.run(host=WEB_SERVER_HOST, port=WEB_SERVER_PORT,
                   cert=WEBHOOK_SSL_CERT, ppk=WEBHOOK_SSL_PRIV)
    else:
        webApp.run(host=WEB_SERVER_HOST, port=WEB_SERVER_PORT)


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO, stream=sys.stdout)
    main()
