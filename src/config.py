import os
import json5
from os import environ as env
from dotenv import load_dotenv
from client_api.auth_client import AuthClient

load_dotenv()

GAME_CHAT_ID = env['GAME_CHAT_ID']
BOT_TOKEN = env['BOT_TOKEN']
AUTH_URL = env['AUTH_URL']
AUTH_TOKEN = env['AUTH_TOKEN']
BASE_WEBHOOK_URL = env['BASE_WEBHOOK_URL']
WEBHOOK_PATH = env['WEBHOOK_PATH']
WEBHOOK_SECRET = env['WEBHOOK_SECRET'] if 'WEBHOOK_SECRET' in env else ''
WEB_SERVER_HOST = env['WEB_SERVER_HOST'] if 'WEB_SERVER_HOST' in env else None
WEB_SERVER_PORT = env['WEB_SERVER_PORT']
WEBHOOK_SSL_CERT = env['WEBHOOK_SSL_CERT'] if 'WEBHOOK_SSL_CERT' in env else None
WEBHOOK_SSL_PRIV = env['WEBHOOK_SSL_PRIV'] if 'WEBHOOK_SSL_PRIV' in env else None
SELF_SIGN = True if 'SELF_SIGN' in env and env['SELF_SIGN'] == '1' else False
CDN_DOMAIN = env['CDN_DOMAIN'] if 'CDN_DOMAIN' in env else ''
DO_SPACE_ACCESSKEY = env['DO_SPACE_ACCESSKEY'] if 'DO_SPACE_ACCESSKEY' in env else ''
DO_SPACE_SECRET = env['DO_SPACE_SECRET'] if 'DO_SPACE_SECRET' in env else ''
STORAGE_ENDPOINT = env['STORAGE_ENDPOINT'] if 'STORAGE_ENDPOINT' in env else ''


def root_dir():
    cwd_path = os.getcwd()
    if 'src' in cwd_path:
        return os.getcwd() + '/..'
    else:
        return os.getcwd() + '/'


with open(root_dir() + '/data.json', 'r', encoding='utf-8') as config_file:
    data = json5.load(config_file)


auth_client = AuthClient(base_url=AUTH_URL,
                         auth_token=f'Bearer {AUTH_TOKEN}')
