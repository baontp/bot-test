import logging
import asyncio
import logging
import sys
from bot.bot_helper import update_config_data
from bot import bot, dispatcher


######################    MAIN    ######################
async def main() -> None:
    await bot.delete_webhook(drop_pending_updates=True)  # skip_updates = True
    await update_config_data(bot)
    await dispatcher.start_polling(bot)

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO, stream=sys.stdout)
    asyncio.run(main())
