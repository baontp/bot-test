from client_api.api import API


class AuthClient(API):
    def __init__(
            self,
            base_url=None,
            auth_token=None,
            **kwargs):
        super().__init__(base_url=base_url, **kwargs)
        self.session.headers.update({'Authorization': auth_token})

    def startChat(self, user_id, username, chat_id, ref_code, is_premium, photo_url):
        # print('/bot/startChat')
        res = self.post('/bot/startChat',
                        {
                            'user_id': user_id,
                            'is_premium': is_premium,
                            'chat_id': chat_id,
                            'ref_code': ref_code,
                            'username': username,
                            'photo_url': photo_url
                        })

    def get_user_info(self, user_id):
        res = self.get('/bot/getUserInfo',
                       {'user_id': user_id})
        if res['code'] == 0:
            data = res['data']
            return data
        else:
            return None

    def getRefferalCode(self, user_id):
        # print('/bot/getReferralCode')
        try:
            res = self.get('/bot/getReferralCode',
                           {'user_id': user_id})
        except:
            return None, None

        if res['code'] == 0:
            data = res['data']
            return ((data['refCode'], data['clubRefCode'])
                    if 'clubRefCode not in data'
                    else data['refCode'])
        else:
            return None, None

    def user_join_chat(self, user_id, chat_id):
        print('/bot/joinChat', user_id, chat_id)
        res = self.post('/bot/joinChat',
                        {
                            'user_id': user_id,
                            'chat_id': chat_id
                        })

    def user_join_club(self, user_id, club_id, club_name):
        print('/bot/joinClub', user_id, club_id, club_name)
        res = self.post('/bot/joinClub',
                        {
                            'user_id': user_id,
                            'club_id': club_id,
                            'club_name': club_name
                        })

    def user_leave_club(self, user_id, club_id):
        print('/bot/leaveClub', user_id, club_id)
        res = self.post('/bot/leaveClub',
                        {
                            'user_id': user_id,
                            'club_id': club_id
                        })
