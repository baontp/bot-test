import bot.message_handler as message_handler
import bot.message_helper as message_helper
from aiogram import Bot, Dispatcher
from config import BOT_TOKEN


# Dispatcher is a root router
dispatcher = Dispatcher()

# Register startup hook to initialize webhook
dispatcher.include_router(message_handler.m_router)

# Initialize Bot instance with a default parse mode which will be passed to all API calls
bot = Bot(BOT_TOKEN, parse_mode='MarkdownV2')
message_handler.setBot(bot)
message_helper.setBot(bot)
