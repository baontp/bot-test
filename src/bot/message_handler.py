from typing import cast
from bot.bot_helper import get_photo_url
import config
from config import auth_client
from aiogram import F, Bot, Router
from aiogram.filters import (
    CommandStart, Command, CommandObject, ChatMemberUpdatedFilter, IS_MEMBER, IS_NOT_MEMBER
)
from aiogram.types import (
    ChatMemberAdministrator, Message, CallbackQuery,
    ChatMemberUpdated, InlineQueryResultsButton, InlineQuery,
    ChosenInlineResult, InputTextMessageContent, InlineQueryResultArticle
)
from aiogram.utils.keyboard import (
    InlineKeyboardBuilder, InlineKeyboardButton)
from aiogram.enums import (ChatMemberStatus)
from bot.message_helper import (
    get_text_file, reply_command, reply_welcome, get_referral_link
)

from utils import fix_markdownV2_error

m_bot: Bot = None
m_router = Router()


def setBot(bot):
    global m_bot
    m_bot = bot


##################    /start    ##################
@m_router.message(CommandStart())
async def command_start_handler(message: Message, command: CommandObject):
    print('chat id', message.chat.id)
    if message.chat.type == 'private':
        if command.args == 'join_club':
            await join_club(message)
        else:
            user = auth_client.get_user_info(message.from_user.id)
            if 'avatar' in user and len(user['avatar']) > 0:
                photo_url = user['avatar']
            else:
                photo_url = await get_photo_url(m_bot, message)

            auth_client.startChat(user_id=message.from_user.id,
                                  username=message.from_user.username,
                                  is_premium=message.from_user.is_premium,
                                  chat_id=message.chat.id,
                                  ref_code=command.args if command.args is not None else '',
                                  photo_url=photo_url)
            await reply_welcome(message)
            await reply_command('start', message)


##################    /invite    ##################
@m_router.message(Command('invite'))
async def invite_friend(message: Message):
    if message.chat.type == 'private':
        personal_link, team_link = await get_referral_link(
            auth_client,
            m_bot,
            message.from_user.id)

        text = ''
        if personal_link is not None and team_link is not None:
            text = get_text_file('/content/club_invite.txt')
            text = text.replace('{personal_link}', personal_link)
            text = text.replace('{team_link}', team_link)
        elif personal_link is not None:
            text = get_text_file('/content/personal_invite.txt')
            text = text.replace('{personal_link}', personal_link)

        await reply_command('invite', message, custom_text=text)


##################    /quest ##################
@m_router.message(Command('quest'))
async def get_quest_list(message: Message):
    if message.chat.type == 'private':
        await reply_command('quest', message)


##################    /createclub    ##################
@m_router.message(Command('createclub'))
async def create_club(message: Message):
    if message.chat.type == 'private':
        await reply_command('createclub', message)


##################    /joinclub    ##################
@m_router.message(Command('joinclub'))
async def join_club(message: Message):
    if message.chat.type == 'private':
        await reply_command('joinclub', message)


##################    COMMAND TEST    ##################
@m_router.message(Command('gamedev'))
async def gamedev(message: Message):
    if message.chat.type == 'private':
        await reply_command('gamedev', message)


@m_router.message(Command('tonconnect'))
async def tonconnect(message: Message):
    if message.chat.type == 'private':
        await reply_command('tonconnect', message)


@m_router.message(Command('questdev'))
async def questdev(message: Message):
    if message.chat.type == 'private':
        await reply_command('questdev', message)


@m_router.message(Command('myid'))
async def get_my_id(message: Message):
    if message.chat.type == 'private':
        await message.answer(text=f"Your id is `{message.from_user.id}`")


@m_router.message(Command('help'))
async def help(message: Message):
    if message.chat.type == 'private':
        await reply_command('help', message)


##################    CMD CALLBACK    ##################
@m_router.callback_query()
async def main_callback_handler(call: CallbackQuery):
    data = call.data
    if data == 'help':
        await help(call.message)
    elif data == 'create_club':
        await create_club(call.message)


######################    MEMBER UPDATE    ######################
@m_router.chat_member(ChatMemberUpdatedFilter(IS_MEMBER >> IS_NOT_MEMBER))
async def on_user_leave(event: ChatMemberUpdated):
    member = event.old_chat_member
    chat = event.chat
    print('on_user_leave', member.user.username, chat.id)
    auth_client.user_leave_club(member.user.id, chat.id)


@m_router.chat_member(ChatMemberUpdatedFilter(IS_NOT_MEMBER >> IS_MEMBER))
async def on_user_join(event: ChatMemberUpdated):
    member = event.new_chat_member
    chat = event.chat
    if str(chat.id) == config.GAME_CHAT_ID:
        print('user_join_chat', member.user.username, chat.id, chat.title)
        auth_client.user_join_chat(member.user.id, chat.id)


@m_router.my_chat_member()
async def chat_member_updated(event: ChatMemberUpdated):
    if event.new_chat_member.status == ChatMemberStatus.ADMINISTRATOR:
        text = get_text_file('/content/hello_new_group.txt')
        text = text.replace('{bot}', config.data['bot_name'])
        await event.answer(text=fix_markdownV2_error(text))


######################    MESSAGE HANLDE    ######################
@m_router.message(F.text.contains('Referral'))
async def referral_message_handler(message: Message):
    if message.chat.type == 'private':
        await invite_friend(message)


@m_router.message(F.text.contains('https://t.me'))
async def message_link_handler(message: Message):
    http_index = message.text.rfind('/')
    if message.chat.type == 'private' and http_index != -1:
        chat_name = message.text[http_index+1:]
        try:
            my_admin = None
            admins = await m_bot.get_chat_administrators(f'@{chat_name}')
            for i in range(len(admins)):
                admin = cast(ChatMemberAdministrator, admins[i])
                if (admin.user.username == config.data['bot_name']
                        and admin.can_invite_users):
                    my_admin = admin
                    break

            if my_admin is None:
                text = get_text_file('/content/invalid_join_link.txt')
                await message.answer(text=fix_markdownV2_error(text))
                return

            chat = await m_bot.get_chat(f'@{chat_name}')
            if (chat is not None and (chat.type == 'group'
                                      or chat.type == 'supergroup'
                                      or chat.type == 'channel')):
                auth_client.user_join_club(
                    message.from_user.id, chat.id, chat.title)
                await message.answer(text=fix_markdownV2_error(f"{message.from_user.username}, You've joined {chat_name} club"))
            else:
                text = get_text_file('/content/invalid_join_link.txt')
                await message.answer(text=fix_markdownV2_error(text))
        except Exception as e:
            print(e)
            text = get_text_file('/content/invalid_join_link.txt')
            await message.answer(text=fix_markdownV2_error(text))


@m_router.message(F.chat.type == 'private')
async def message_handler(message: Message):
    # print('message_handler', message.text)
    text = get_text_file('/content/default_message.txt')
    await message.answer(fix_markdownV2_error(text))


@m_router.inline_query()
async def inline_query_handler(inline_query: InlineQuery):
    text = inline_query.query
    url_index = text.index('https://t.me')
    end_index = text.index('\n')

    app_url = None
    if url_index > -1 and end_index > -1:
        app_url = text[url_index:end_index]

    start_param = app_url[app_url.rindex(
        '=')+1:] if app_url is not None else None
    inline_data = config.data['inline_data']
    button_data = inline_data['button']
    button = InlineQueryResultsButton(text=button_data['text'],
                                      start_parameter=start_param)
    input_message_content = InputTextMessageContent(
        message_text=fix_markdownV2_error(text))

    kb_builder = InlineKeyboardBuilder()
    kb_builder.add(InlineKeyboardButton(text=button_data['text'],
                                        url=app_url))

    results_data = inline_data['results']
    results = []
    if 'article' in results_data:
        article_data = results_data['article']
        results.append(InlineQueryResultArticle(id=f'{inline_query.from_user.id}',
                                                reply_markup=kb_builder.as_markup(),
                                                thumbnail_url=article_data['thumb_url'],
                                                title=article_data['title'],
                                                input_message_content=input_message_content)
                       )
    await inline_query.answer(button=button,
                              results=results)


@m_router.chosen_inline_result()
async def chosen_inline_result_handler(chosen_inline_result: ChosenInlineResult):
    print('chosen_inline_result_handler',
          chosen_inline_result.from_user.username,
          chosen_inline_result.query)
