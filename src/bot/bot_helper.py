from config import auth_client
from aiogram import Bot
from aiogram.types import Message
import requests
import boto3.session
import botocore
import config

m_photo_url_template = f'https://api.telegram.org/file/bot{config.BOT_TOKEN}'

CDN_DOMAIN = config.CDN_DOMAIN
DO_SPACE_ACCESSKEY = config.DO_SPACE_ACCESSKEY
DO_SPACE_SECRET = config.DO_SPACE_SECRET
STORAGE_ENDPOINT = config.STORAGE_ENDPOINT


async def update_config_data(bot: Bot):
    data = config.data
    bot_name = (await bot.me()).username
    data['bot_name'] = bot_name
    data['bot_url'] = f"https://t.me/{bot_name}"
    game_app = data['game_app']
    game_app['url'] = f"{data['bot_url']}{game_app['url']}"
    quest_app = data['quest_app']
    quest_app['url'] = f"{data['bot_url']}{quest_app['url']}"


def upload_photo(userId, image_data):

    # Step 2: The new session validates your request and directs it to your Space's specified endpoint using the AWS SDK.
    session = boto3.session.Session()
    client = session.client('s3',
                            # Find your endpoint in the control panel, under Settings. Prepend "https://".
                            endpoint_url=STORAGE_ENDPOINT,
                            # Configures to use subdomain/virtual calling format.
                            config=botocore.config.Config(
                                s3={'addressing_style': 'virtual'}),
                            # Use the region in your endpoint.
                            region_name='ap-southeast-1',
                            # Access key pair. You can create access key pairs using the control panel or API.
                            aws_access_key_id=DO_SPACE_ACCESSKEY,
                            aws_secret_access_key=DO_SPACE_SECRET)  # Secret access key defined through an environment variable.

    # Call the put_object command and specify the file to upload.
    try:
        photo_key = f'userAvatar/{userId}.png'
        ret = client.put_object(
            Bucket='tma-storage',
            Key=photo_key,
            Body=image_data,
            ACL='public-read',
            Metadata={'x-amz-meta-my-key': 'your-value'}
        )
        return f'{CDN_DOMAIN}/{photo_key}'
    except:
        return f''


async def get_photo_url(bot: Bot, message: Message):
    data = await bot.get_user_profile_photos(message.from_user.id)
    if len(data.photos) == 0:
        return ''

    photos = data.photos[0]
    photo_file_id = photos[0].file_id if len(photos) > 0 else None
    file = None
    if photo_file_id is not None:
        file = await bot.get_file(photo_file_id)

    photo_url = f'{m_photo_url_template}/{file.file_path}' if file is not None else ''
    response = requests.get(photo_url, stream=True)
    if not response.ok:
        print(response)

    photo_url = upload_photo(message.from_user.id, response.content)
    return photo_url
