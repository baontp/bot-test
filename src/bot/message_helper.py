from aiogram import Bot
from client_api.auth_client import AuthClient
import config
from aiogram.types import Message, BufferedInputFile, WebAppInfo
from aiogram.utils.keyboard import (
    InlineKeyboardBuilder, ReplyKeyboardBuilder, InlineKeyboardButton,
    KeyboardButton, SwitchInlineQueryChosenChat)
from aiogram.methods import SendMessage
from aiogram.utils.deep_linking import create_start_link, create_startgroup_link
from utils import fix_markdownV2_error, root_dir
from config import auth_client

m_bot = None


def setBot(bot):
    global m_bot
    m_bot = bot


async def reply_welcome(message: Message) -> SendMessage:
    data = config.data
    if 'reply_buttons' in data:
        reply_buttons = data['reply_buttons']
        kb_builder = ReplyKeyboardBuilder()
        for row in reply_buttons:
            buttons = [create_reply_button(d) for d in row]
            kb_builder.row(*buttons)

    await message.answer(text=fix_markdownV2_error(data['welcome_text']),
                         reply_markup=kb_builder.as_markup(resize_keyboard=True))


async def reply_command(
    command: str,
    message: Message,
    custom_text: str = None
) -> SendMessage:

    command_data = get_command_data(command)
    if command_data is None:
        return None

    personal_link, team_link = None, None
    if 'share_link' in command_data and command_data['share_link']:
        personal_link, team_link = await get_referral_link(
            auth_client,
            m_bot,
            message.from_user.id)

    buttons = []
    kb_builder = None
    if 'buttons' in command_data:
        buttons_data = command_data['buttons']
        kb_builder = InlineKeyboardBuilder()
        for row in buttons_data:
            buttons = [create_inline_button(
                d, personal_link, team_link) for d in row]
            kb_builder.row(*buttons)

    text = custom_text
    if text is None:
        if 'text_file' in command_data:
            with open(root_dir() + command_data['text_file'], 'r', encoding='utf-8') as f:
                text = f.read()

        if 'text' in command_data:
            text = command_data['text']

    photo = None
    if 'photo' in command_data:
        photo_path = root_dir() + command_data['photo']
        photo = BufferedInputFile.from_file(path=photo_path)

    if photo is not None:
        await message.answer_photo(photo=photo,
                                   caption=fix_markdownV2_error(text),
                                   reply_markup=kb_builder.as_markup() if kb_builder is not None else None)
    else:
        await message.answer(text=fix_markdownV2_error(text),
                             reply_markup=kb_builder.as_markup() if kb_builder is not None else None)


def create_reply_button(button_data: dict):
    web_app_url = get_url(button_data, 'web_app')
    web_app = WebAppInfo(url=web_app_url) if web_app_url is not None else None
    return KeyboardButton(text=button_data['text'], web_app=web_app)


def create_inline_button(button_data: dict,
                         personal_link: str, team_link: str):
    text = button_data['text']

    web_app_url = get_url(button_data, 'web_app')
    web_app = WebAppInfo(url=web_app_url) if web_app_url is not None else None
    url = get_url(button_data, 'url')
    callback_data = button_data['callback'] if 'callback' in button_data else None

    chat_content = None
    chosen_chat = None
    if 'chat_content_file' in button_data:
        with open(root_dir() + button_data['chat_content_file'], 'r', encoding='utf-8') as f:
            chat_content = f.read()
            if '{personal_link}' in chat_content and personal_link is not None:
                chat_content = chat_content.replace(
                    '{personal_link}', personal_link)

            if '{team_link}' in chat_content and team_link is not None:
                chat_content = chat_content.replace(
                    '{team_link}', team_link)

        chosen_chat = SwitchInlineQueryChosenChat(query=chat_content,
                                                  allow_bot_chats=False,
                                                  allow_user_chats=True,
                                                  allow_group_chats=True,
                                                  allow_channel_chats=True)

    return InlineKeyboardButton(text=text,
                                web_app=web_app,
                                url=url,
                                switch_inline_query_chosen_chat=chosen_chat,
                                callback_data=callback_data)


def get_text_file(text_file: str):
    with open(root_dir() + text_file, 'r', encoding='utf-8') as f:
        return f.read()


def get_command_data(command: str):
    data = config.data
    for cmd in data['commands']:
        if cmd['name'] == command:
            command_data = cmd
            break

    if command_data is None:
        print(f'Command {command} not found')

    return command_data


def get_url(button_data, key):
    url_ref = button_data[key] if key in button_data else None

    if url_ref is not None and url_ref[0] == '$':
        if url_ref[1] == '{':
            url_ref = url_ref[2:-1]
            urls = url_ref.split('.')
            url_ref = config.data[urls[0]][urls[1]]

    return url_ref


async def get_referral_link(auth_client: AuthClient, bot: Bot, user_id):
    bot_url = config.data['bot_url']
    ref_code, club_ref_code = auth_client.getRefferalCode(user_id)
    if ref_code is not None:
        personal_link = f"{bot_url}?start={ref_code}"
        team_link = (f"{bot_url}?start={club_ref_code}_{user_id}"
                     if club_ref_code is not None
                     else None)
        return personal_link, team_link

    return None, None
