import ssl
from aiogram import Bot
from aiohttp import web
from aiohttp.web import Request
from aiohttp.web_response import json_response
from utils import fix_markdownV2_error
from aiogram.utils.keyboard import (
    InlineKeyboardBuilder, ReplyKeyboardBuilder, InlineKeyboardButton,
    KeyboardButton, SwitchInlineQueryChosenChat)
from aiogram.types import Message, BufferedInputFile, WebAppInfo


def create_inline_button(button_data: dict):
    text = button_data['text']
    web_app_url = button_data['web_app_url'] if 'web_app_url' in button_data else None
    web_app = WebAppInfo(url=web_app_url) if web_app_url is not None else None
    url = button_data['url'] if 'url' in button_data else None
    callback_data = button_data['callback'] if 'callback' in button_data else None
    return InlineKeyboardButton(text=text,
                                web_app=web_app,
                                url=url,
                                callback_data=callback_data)


class WebApp:

    def __init__(self, bot: Bot) -> None:
        self.bot = bot
        self.app = web.Application()
        self.app.router.add_post('/push_message', self.push_message)

    def run(self, host, port, cert=None, ppk=None):

        if cert is not None and ppk is not None:
            # Generate SSL context
            context = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
            context.load_cert_chain(cert, ppk)
        else:
            context = None

        # And finally start webserver
        web.run_app(self.app,
                    host=host,
                    port=int(port),
                    ssl_context=context)

    async def push_message(self, request: Request):
        if request.can_read_body:
            data = await request.json()
            try:
                chat_ids = data['chat_id']
                message = data['message']

                kb_builder = None
                if 'buttons' in data:
                    buttons_data = data['buttons']
                    kb_builder = InlineKeyboardBuilder()
                    for row in buttons_data:
                        buttons = [create_inline_button(d) for d in row]
                        kb_builder.row(*buttons)

                await self.bot.send_message(chat_id=chat_ids,
                                            text=fix_markdownV2_error(message),
                                            reply_markup=kb_builder.as_markup() if kb_builder is not None else None)
            except ValueError as err:
                print(err)
                return json_response({"ok": -1, "error": err.__str__()})

        return json_response({"result": 0})
